<?php
require_once 'vendor/autoload.php';
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

include_once 'inc/aws-config.php';

function pullS3Images($config) {
    $bucket = $config['s3bucket'];
    $s3 = new S3Client([
        'version' => 'latest',
        'region' => $config['s3region'],
        'credentials' => [
            'key' => $config["s3access_key"],
            'secret' => $config["s3secret_key"]
        ]
    ]);

    try {
        $objects = $s3->listObjects([
            'Bucket' => $bucket
        ]);
        foreach ($objects['Contents'] as $object) {
            $imgURL = 'http://'.$bucket.'.s3.amazonaws.com/'. $object['Key'];
            echo "<li>
                    <img src='$imgURL' />
                   </li>";
        }
    } catch (S3Exception $e) {
        echo $e->getMessage() . PHP_EOL;
    }
}


?>

<div>
    <ul>
        <?php pullS3Images($aws_config); ?>
    </ul>
</div>
