# Dropzone S3 Demo

This application is an example of the php aws sdk in use with dropzone.js 
## Installation

Make sure you have composer installed. If not you can find instructions to install it [here](https://getcomposer.org/).

Install dependencies:
```bash
composer install 
```

## AWS Configuration

Copy inc/aws-config.example.php and rename it aws-config.php.

Place your S3 bucket configuration values in the $aws_config associative array.
```php
$aws_config = array(
    "s3bucket" => "", // S3 Bucket Name
    "s3region" => "", // S3 Region, e.g us-west-1
    "s3access_key" => "", // S3 Access Key
    "s3secret_key" => "" // S3 Secret Key
);
```

## AWS Permissions
Your S3 bucket policy should allow read access. This can be authenticated or restricted to a particular IP.

