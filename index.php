
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Dropzone</title>
	<link rel="stylesheet" href="lib/dropzone.css" />
</head>

<body>
	<div>
		<form enctype="multipart/form-data" action="upload.php" class="dropzone" id="dropzoneWidget">

		</form>
	</div>
	<?php include_once 'pull.php'?>
	<script src="lib/dropzone.js"></script>
</body>


</html>
