<?php
require_once 'vendor/autoload.php';

include_once 'push.php';
include_once 'inc/aws-config.php';

$target_dir = "uploads/";
if(!is_dir($target_dir)) {
    mkdir($target_dir);
}
$target_file = $target_dir . basename($_FILES["file"]["name"]);
move_uploaded_file($_FILES["file"]["tmp_name"], $target_dir.$_FILES['file']['name']);

pushToS3($_FILES["file"]["name"], $aws_config);
// Delete temp file
unlink($target_file);


