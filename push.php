<?php
require_once 'vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

function pushToS3($file, $config) {

    $target_dir = 'uploads/';
    $target_file = $target_dir . $file;
    if(file_exists($target_file)) {
        $key = $file;
    } else {
        die("file does not exist");
    }
    $bucketName = $config["s3bucket"]?: die("NO S3 BUCKET NAME");
    try {
        $s3 = new S3Client([
            'version' => 'latest',
            'region' => $config["s3region"],
            'credentials' => [
                'key' => $config["s3access_key"],
                'secret' => $config["s3secret_key"]
            ]
        ]);
        $result = $s3->putObject([
            'Bucket' => $bucketName,
            'Key' => $key,
            'SourceFile' => $target_file
        ]);
        echo $result['ObjectURL'] . PHP_EOL;
    } catch (S3Exception $e) {
        echo 'There was an error uploading the file';
        echo $e->getMessage();
    }

}