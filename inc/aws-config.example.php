<?php
// This is an example of your AWS configuration. Copy it and rename it to aws-config.php. Then fill it with your bucket info.
$aws_config = array(
    "s3bucket" => "", // S3 Bucket Name
    "s3region" => "", // S3 Region, e.g us-west-1
    "s3access_key" => "", // S3 Access Key
    "s3secret_key" => "" // S3 Secret Key
);
